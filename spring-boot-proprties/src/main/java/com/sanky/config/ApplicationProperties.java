package com.sanky.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "myApp", ignoreUnknownFields = false)
public class ApplicationProperties {



	private final Cache cache = new Cache();

	private final Mail mail = new Mail();

	private final Security security = new Security();


	public Cache getCache() {
		return cache;
	}

	public Mail getMail() {
		return mail;
	}

	public Security getSecurity() {
		return security;
	}



	public static class Cache {
		private final Ehcache ehcache = new Ehcache();

		public Ehcache getEhcache() {
			return ehcache;
		}

		public static class Ehcache {

			private String maxBytesLocalHeap = "16M";

			public String getMaxBytesLocalHeap() {
				return maxBytesLocalHeap;
			}

			public void setMaxBytesLocalHeap(String maxBytesLocalHeap) {
				this.maxBytesLocalHeap = maxBytesLocalHeap;
			}
		}
	}

	public static class Mail {

		private String from = "LeaseWallet@localhost";

		private String baseUrl = "";

		private String mobileBaseUrl = "";

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getBaseUrl() {
			return baseUrl;
		}

		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}

		public String getMobileBaseUrl() {
			return mobileBaseUrl;
		}

		public void setMobileBaseUrl(String mobileBaseUrl) {
			this.mobileBaseUrl = mobileBaseUrl;
		}

		@Override
		public String toString() {
			return "Mail [from=" + from + ", baseUrl=" + baseUrl + ", mobileBaseUrl=" + mobileBaseUrl + "]";
		}


	}

	public static class Security {

		private final Authentication authentication = new Authentication();

		public Authentication getAuthentication() {
			return authentication;
		}
		public static class Authentication {

			private final Jwt jwt = new Jwt();

			public Jwt getJwt() {
				return jwt;
			}

			public static class Jwt {

				private String secret;

				private long tokenValidityInSeconds = 1800;

				private long tokenValidityInSecondsForRememberMe = 2592000;

				public String getSecret() {
					return secret;
				}

				public void setSecret(String secret) {
					this.secret = secret;
				}

				public long getTokenValidityInSeconds() {
					return tokenValidityInSeconds;
				}

				public void setTokenValidityInSeconds(long tokenValidityInSeconds) {
					this.tokenValidityInSeconds = tokenValidityInSeconds;
				}

				public long getTokenValidityInSecondsForRememberMe() {
					return tokenValidityInSecondsForRememberMe;
				}

				public void setTokenValidityInSecondsForRememberMe(long tokenValidityInSecondsForRememberMe) {
					this.tokenValidityInSecondsForRememberMe = tokenValidityInSecondsForRememberMe;
				}
			}
		}
	}

}
