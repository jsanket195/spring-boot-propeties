package com.sanky.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sanky.config.ApplicationProperties;

@RestController
@RequestMapping("/properties")
public class Application {
	
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@GetMapping("/mail")
	public String getProperties() {
		return applicationProperties.getMail().toString();
	}
	
}
