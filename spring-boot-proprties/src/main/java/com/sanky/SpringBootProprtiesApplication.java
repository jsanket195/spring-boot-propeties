package com.sanky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import com.sanky.config.ApplicationProperties;


/**
 * 
 * @author Sanky
 *
 */
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties({ ApplicationProperties.class})
public class SpringBootProprtiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootProprtiesApplication.class, args);
	}
}
